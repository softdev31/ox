/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.oxprogram;

import java.util.Scanner;
import ox.OX;

/**
 *
 * @author ASUS
 */
public class OXProgram {

    public static void main(String[] args) {
        OX ox = new OX();
        Scanner kb = new Scanner(System.in);
        int check;
        System.out.println("Welcome to OX Game");
        while (true) {
            ox.showtable();
            ox.input();
            int row = kb.nextInt();
            int col = kb.nextInt();
            if (ox.checkbug(row, col) == true) {
                ox.showXO(row, col);
                ox.turn();
                check = ox.checkwin();
                if (check == 1) {
                    ox.showtable();
                    ox.showwinO();
                    break;
                } else if (check == 2) {
                    ox.showtable();
                    ox.showwinX();
                    break;
                } else if (check == 3) {
                    ox.showtable();
                    ox.showDraw();
                    break;
                }
            }
        }
    }
}
