/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ox;

/**
 *
 * @author ASUS
 */
public class OX {

    private String[][] table = {
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}
    };
    private int turn = 0;
    private int count = 0;
    public void showtable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public void input() {
        checkturn();
        System.out.println("Please input row, col:");
    }
    
    public boolean checkbug(int row, int col) {
        if(row <= 3 && row >= 1 && col <= 3 && col >= 1 && table[row - 1][col - 1].equals("-") ){
            return true;
        }else{
            System.out.println("\nError !!!! \n");
            return false;
        }
    }

    private void checkturn() {
        if (turn == 0) {
            System.out.println("Turn O");
        } else {
            System.out.println("Turn X");
        }
    }

    public int showXO(int row, int col) {
        if (turn == 0) {
            table[row - 1][col - 1] = "O";
        } else {
            table[row - 1][col - 1] = "X";
        }
        System.out.println("");
        return 0;
    }
   

    public void turn() {
        if (turn == 0) {
            turn = turn + 1;
        } else {
            turn = turn - 1;
        }
    }

    public int checkwin() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i].equals(table[1][i]) && table[0][i].equals(table[2][i]) && !table[0][i].equals("-")) {
                if (table[0][i].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            } else if (table[i][0].equals(table[i][1]) && table[i][0].equals(table[i][2]) && !table[i][0].equals("-")) {
                if (table[i][0].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            }
        }
        if (table[0][0].equals(table[1][1]) && table[0][0].equals(table[2][2]) && !table[0][0].equals("-")) {
                if (table[0][0].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            } else if (table[0][2].equals(table[1][1]) && table[0][2].equals(table[2][0]) && !table[0][2].equals("-")) {
                if (table[0][2].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            }
        count++;
        if(count == 9){
            return 3;
        }
        return 0;
    }
    
    public void showwinX() {
        System.out.println("");
        System.out.println(">>>X Win<<<");
    }
    public void showwinO() {
        System.out.println("");
        System.out.println(">>>O Win<<<");
    }
    public void showDraw() {
        System.out.println("");
            System.out.println(">>>Draw<<<");
    }

}
